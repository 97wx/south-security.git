package org.nm.security.config;

import org.nm.security.filter.TimeFilter;
import org.nm.security.interceptor.TimeInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/10/7
 * Time: 21:44
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
/*@Configuration*/
public class WebConfig extends WebMvcConfigurationSupport {

    @SuppressWarnings("unused")
    @Autowired
    private TimeInterceptor timeInterceptor;

    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        //registry.addInterceptor(timeInterceptor).addPathPatterns("/*");
        // super.addInterceptors(registry);
    }

    //@Bean
    public FilterRegistrationBean timeFilter() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        TimeFilter timeFilter = new TimeFilter();
        filterRegistrationBean.setFilter(timeFilter);
        List<String> urls = new ArrayList<>();
        urls.add("/");
        filterRegistrationBean.setUrlPatterns(urls);
        return filterRegistrationBean;
    }

}
