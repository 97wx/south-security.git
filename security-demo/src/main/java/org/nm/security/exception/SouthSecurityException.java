package org.nm.security.exception;

import java.io.Serializable;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/10/7
 * Time: 16:08
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
public class SouthSecurityException extends Exception implements Serializable {
    private String id;

    public SouthSecurityException(String message, String id) {
        super(message);
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
