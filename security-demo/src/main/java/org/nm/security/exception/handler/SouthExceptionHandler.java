package org.nm.security.exception.handler;

import org.nm.security.exception.SouthSecurityException;
import org.nm.security.model.Result;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/10/7
 * Time: 16:11
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
@ControllerAdvice
public class SouthExceptionHandler {

    @ExceptionHandler(SouthSecurityException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Result southSecurityHandler(SouthSecurityException southSecurityException) {
        Result result = new Result();
        result.setData(southSecurityException.getId());
        return result;
    }
}
