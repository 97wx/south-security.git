package org.nm.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/10/7
 * Time: 15:49
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
@SpringBootApplication
public class SouthSecurityApplication {
    public static void main(String[] args) {
        SpringApplication.run(SouthSecurityApplication.class, args);
    }
}
