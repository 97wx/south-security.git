package org.nm.security.authorize;

import org.nm.security.core.authorize.AuthorizeConfigProvider;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.stereotype.Component;


/**
 * 单独授权的配置
 * anyRequest 一定要在最后读取
 * 1、如果先读取当前这个配置文件，则其他配置有可能不第生效
 * 2、公用的配置一定要先读 {@link org.nm.security.core.authorize.NmAuthorizeConfigProvider#config(ExpressionUrlAuthorizationConfigurer.ExpressionInterceptUrlRegistry)}
 * 3、使用order 配置其调用顺序
 */
@Component
@Order(Integer.MAX_VALUE)
public class DemoAuthorizeConfigProvider implements AuthorizeConfigProvider {
    @Override
    public void config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config) {
        config.antMatchers(
                "/user/validate"
        ).permitAll().anyRequest().access("@rbacService.hashPermision(request,authentication)");
    }
}
