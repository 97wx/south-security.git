package org.nm.security.entity;

import com.fasterxml.jackson.annotation.JsonView;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/10/7
 * Time: 16:18
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
public class User implements Serializable {
    public interface UserSimpleView {
    }

    public interface UserDetailView extends UserSimpleView {
    }

    private Integer id;
    @NotNull
    private String name;
    @NotNull
    private String password;
    private Integer age;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonView(UserSimpleView.class)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonView(UserDetailView.class)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
