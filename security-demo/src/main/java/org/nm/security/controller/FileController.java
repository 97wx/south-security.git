package org.nm.security.controller;

import org.apache.commons.io.IOUtils;
import org.nm.security.model.FileInfo;
import org.nm.security.model.Result;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Date;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/10/7
 * Time: 22:57
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
@RestController
@RequestMapping("/file")
public class FileController {

    String folder = "E:\\gitee\\south-security\\security-demo\\src\\main\\java\\org\\nm\\security\\controller";

    @PostMapping("/upload")
    public Result<FileInfo> uploadFile(MultipartFile file) throws IOException {
        System.out.println(file.getName());
        System.out.println(file.getOriginalFilename());

        File localFile = new File(folder, new Date().getTime() + ".txt");
        file.transferTo(localFile);
        return Result.success(new FileInfo(localFile.getAbsolutePath()));
    }

    @GetMapping("/download/{id:\\d+}")
    public void download(@PathVariable String id, HttpServletRequest request, HttpServletResponse response) {
        try (InputStream inputStream = new FileInputStream(folder + "\\" + id + ".txt");
             OutputStream outputStream = response.getOutputStream();
        ) {
            response.setContentType("application/x-download");
            response.setHeader("Content-Disposition", "attachment;filename=test.txt");
            IOUtils.copy(inputStream, outputStream);
            outputStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
