package org.nm.security.controller;

import com.fasterxml.jackson.annotation.JsonView;
import org.nm.security.core.validate.code.NextTokenRepository;
import org.nm.security.core.validate.code.ValidateCode;
import org.nm.security.entity.User;
import org.nm.security.exception.SouthSecurityException;
import org.nm.security.model.FailMsg;
import org.nm.security.model.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.ServletWebRequest;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/10/7
 * Time: 15:52
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
@RestController
public class UserController {
    @Autowired
    private NextTokenRepository nextTokenRepository;

    /**
     * 获取用户信息
     *
     * @param authentication
     * @return
     */
    @GetMapping("/user/me")
    public Object me(Authentication authentication) {
        return authentication;
    }


    /**
     * 增
     *
     * @param user
     * @return
     */
    @PostMapping("/user")
    public Result<List<FailMsg>> save(@RequestBody @Valid User user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            List<FailMsg> list = bindingResult.getFieldErrors().stream().map(fe -> {
                FailMsg failMsg = new FailMsg(fe.getField(), fe.getDefaultMessage());
                return failMsg;
            }).collect(Collectors.toList());
            return Result.error(list);
        }
        return Result.success();
    }

    /**
     * 删
     *
     * @param id
     * @return
     */
    @DeleteMapping("/user/{id:\\d+}")
    public Result delete(@PathVariable Integer id) {
        //todo delete operation

        return Result.success();
    }

    /**
     * 修改
     *
     * @param user
     * @return
     */
    @PutMapping("/user/{id:\\d+}")
    public Result<List<FailMsg>> update(@RequestBody @Valid User user, BindingResult bindingResult) {
        //todo update operation
        if (bindingResult.hasErrors()) {
            List<FailMsg> list = bindingResult.getFieldErrors().stream().map(fe -> {
                FailMsg failMsg = new FailMsg(fe.getField(), fe.getDefaultMessage());
                return failMsg;
            }).collect(Collectors.toList());
            return Result.error(list);
        }
        return Result.success();
    }

    /**
     * 查
     *
     * @param id
     * @return
     */
    @GetMapping("/user/{id:\\d+}")
    public Result<User> get(@PathVariable Integer id) {
        User user = new User();
        user.setId(id);
        user.setName("123");
        return Result.success(user);
    }

    /**
     * 使用jsionView
     *
     * @param id
     * @return
     */
    @GetMapping("/user/detail/{id:\\d+}")
    @JsonView(User.UserSimpleView.class)
    public User getDetail(@PathVariable Integer id) {
        User user = new User();
        user.setId(id);
        user.setName("123");
        return user;
    }

    /**
     * 抛出异常，测试
     *
     * @param id
     * @return
     * @throws SouthSecurityException
     * @See SouthExceptionHandler
     */
    @GetMapping("/user/error/{id:\\d+}")
    public Result<User> error(@PathVariable Integer id) throws SouthSecurityException {
        throw new SouthSecurityException("123", id.toString());
    }

    /**
     * 获取校验码的token
     *
     * @param request
     * @return
     * @throws SouthSecurityException
     */
    @GetMapping("/user/validate")
    public Result<ValidateCode> validate(ServletWebRequest request) {
        ValidateCode validateCode = nextTokenRepository.get(request);
        return Result.success(validateCode);
    }


}
