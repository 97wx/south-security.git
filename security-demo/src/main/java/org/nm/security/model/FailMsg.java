package org.nm.security.model;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/10/7
 * Time: 17:56
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
public class FailMsg {
    private String field;
    private String message;

    public FailMsg() {
    }

    public FailMsg(String field, String message) {
        this.field = field;
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
