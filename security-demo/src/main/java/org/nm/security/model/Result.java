package org.nm.security.model;

import java.io.Serializable;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/10/7
 * Time: 16:00
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
public class Result<T> implements Serializable {
    private Integer status;
    private String message;
    private T data;

    public static <T> Result<T> success() {
        Result result = new Result();
        result.setStatus(0);
        return result;
    }

    public static <T> Result success(T object) {
        Result<T> result = success();
        result.setData(object);
        return result;
    }

    public static <T> Result<T> error() {
        Result result = new Result();
        result.setStatus(1);
        return result;
    }

    public static <T> Result<T> error(T t) {
        Result result = error();
        result.setData(t);
        return result;
    }

    public static <T> Result<T> error(T t, Integer status) {
        Result result = error(t);
        result.setStatus(status);
        return result;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
