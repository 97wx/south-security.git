package org.nm.security.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/10/7
 * Time: 22:59
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class FileControllerTest {
    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;


    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void uploadFile() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/file/upload")
                .file(new MockMultipartFile("file", "test.txt", "multipart/form-data", "hello".getBytes("UTF-8")))
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }
}
