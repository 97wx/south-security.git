package org.nm.security.app.validate.code;

import org.apache.commons.lang.StringUtils;
import org.nm.security.core.validate.code.ValidateCode;
import org.nm.security.core.validate.code.ValidateCodeException;
import org.nm.security.core.validate.code.ValidateCodeRepository;
import org.nm.security.core.validate.code.ValidateCodeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.ServletWebRequest;

import java.util.concurrent.TimeUnit;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/12/23
 * Time: 15:39
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
@Component
public class RedisValidateCodeRepository implements ValidateCodeRepository {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;


    @Override
    public void save(ServletWebRequest request, ValidateCode validateCode, ValidateCodeType validateCodeType) {
        redisTemplate.opsForValue().set(buildKey(request, validateCodeType, validateCode.getCode()), validateCode, 30, TimeUnit.MINUTES);
    }

    @Override
    public ValidateCode get(ServletWebRequest request, ValidateCodeType validateCodeType) {
        try {
            String codeInRequest = ServletRequestUtils.getStringParameter(request.getRequest(), validateCodeType.getParamNameOnValidate());
            Object value = redisTemplate.opsForValue().get(buildKey(request, validateCodeType, codeInRequest));
            if (value != null) {
                return (ValidateCode) value;
            }
        } catch (ServletRequestBindingException e) {
            logger.error("获取validateCode错误：", e);
        }
        return null;
    }

    @Override
    public void remove(ServletWebRequest request, ValidateCodeType validateCodeType) {
        try {
            String codeInRequest = ServletRequestUtils.getStringParameter(request.getRequest(), validateCodeType.getParamNameOnValidate());
            redisTemplate.delete(buildKey(request, validateCodeType, codeInRequest));
        } catch (ServletRequestBindingException e) {
            logger.error("获取validateCode错误：", e);
        }
    }


    private String buildKey(ServletWebRequest request, ValidateCodeType validateCodeType, String code) {
        String deviceId = request.getHeader("deviceId");
        if (!StringUtils.isNotBlank(deviceId)) {
            throw new ValidateCodeException("请在请求头中携带deviceId");
        }
        return "code:" + validateCodeType.toString().toUpperCase() + ":" + deviceId + ":" + code.toUpperCase();
    }
}
