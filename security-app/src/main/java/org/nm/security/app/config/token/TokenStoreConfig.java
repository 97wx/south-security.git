package org.nm.security.app.config.token;

import org.nm.security.app.holder.NmJwtTokenEnhancer;
import org.nm.security.core.properties.SecurityProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;

@Configuration
public class TokenStoreConfig {
    @Autowired
    private RedisConnectionFactory redisConnectionFactory;

    /**
     * 配置redis存储token
     *
     * @param @return
     * @return TokenStore
     * @throws
     * @Description: 配置文件有 nm.security.oauth2.storeType = redis 时才生效
     */
    @Bean
    @ConditionalOnProperty(prefix = "nm.security.oauth2", name = "storeType", havingValue = "redis")
    public TokenStore redisTokenStore() {
        return new RedisTokenStore(redisConnectionFactory);
    }


    /**
     * JWT配置
     * ClassName: JwtTokenConfig
     *
     * @Description:\
     * @ConditionalOnProperty是说，有前缀imooc.security.oauth2.storeType = jwt 的配置时，这个类里的配置才生效
     * matchIfMissing =true 意思是当配置文件里不配置imooc.security.oauth2.storeType = jwt时，配置是生效的
     */
    @Configuration
    @ConditionalOnProperty(prefix = "nm.security.oauth2", name = "storeType", havingValue = "jwt", matchIfMissing = true)
    public static class JwtTokenConfig {

        @Autowired
        private SecurityProperties securityProperties;

        /**
         * 配置jwt
         *
         * @return
         */
        @Bean
        public TokenStore jwtTokenStore() {
            return new JwtTokenStore(jwtAccessTokenConverter());
        }

        /**
         * 给JWT加签名
         *
         * @return
         */
        @Bean
        public JwtAccessTokenConverter jwtAccessTokenConverter() {
            JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
            jwtAccessTokenConverter.setSigningKey(securityProperties.getOauth2().getJwtSigningKey());//jwt签名
            return jwtAccessTokenConverter;
        }

        /**
         * WT增强器
         *
         * @return
         */
        @Bean
        @ConditionalOnMissingBean(name = "jwtTokenEnhancer")
        public TokenEnhancer jwtTokenEnhancer() {
            return new NmJwtTokenEnhancer();
        }
    }
}
