package org.nm.security.app.config.security;

import org.nm.security.app.authentication.NmAuthenticationFailureHandler;
import org.nm.security.app.authentication.NmAuthenticationSuccessHandler;
import org.nm.security.core.authentication.mobile.SmsCodeAuthenticationSecurityConfig;
import org.nm.security.core.authorize.AuthorizeConfigManager;
import org.nm.security.core.properties.SecurityConstants;
import org.nm.security.core.validate.code.ValidateCodeSecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/12/16
 * Time: 23:36
 * 码云：https://gitee.com/97wx/watched
 * ========================
 * 资源服务器
 */
@Configuration
@EnableResourceServer
public class NmResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Autowired
    private NmAuthenticationSuccessHandler nmAuthenticationSuccessHandler;

    @Autowired
    private NmAuthenticationFailureHandler nmAuthenticationFailureHandler;

    @Autowired
    private SmsCodeAuthenticationSecurityConfig smsCodeAuthenticationSecurityConfig;

    @Autowired
    private AuthorizeConfigManager authorizeConfigManager;

    @Autowired
    private ValidateCodeSecurityConfig validateCodeSecurityConfig;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        //实现需要认证的接口跳转表单登录,安全=认证+授权
        //http.httpBasic() //这个就是默认的弹框认证
        http.formLogin() //表单认证
                .loginPage(SecurityConstants.DEFAULT_UNAUTHENTICATION_URL)
                .loginProcessingUrl(SecurityConstants.DEFAULT_LOGIN_PROCESSING_URL_FORM)
                .successHandler(nmAuthenticationSuccessHandler) //自定义的认证后处理器
                .failureHandler(nmAuthenticationFailureHandler)
                .and()
                .apply(validateCodeSecurityConfig).//把验证码过滤器加载登录过滤器前边
                and()
                .apply(smsCodeAuthenticationSecurityConfig)
                .and()
                //.apply(nmSocialSecurityConfig)
                //.and()
                .csrf().disable();  //关闭csrf 防护
        //授权管理器
        authorizeConfigManager.config(http.authorizeRequests());
    }

}
