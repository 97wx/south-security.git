package org.nm.security.browser.support;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/11/23
 * Time: 22:05
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
public class SimpleResponse {
    public SimpleResponse(Object content) {
        this.content = content;
    }

    private Object content;

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

}
