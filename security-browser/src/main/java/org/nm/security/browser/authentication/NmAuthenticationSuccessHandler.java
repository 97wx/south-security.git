package org.nm.security.browser.authentication;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.nm.security.core.properties.LoginType;
import org.nm.security.core.properties.SecurityProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/11/22
 * Time: 22:34
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
@Component("nmAuthenticationSucessHandler")
public class NmAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private SecurityProperties securityProperties;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        logger.info("登录成功");
        /**
         * 判断配置的登录类型，做不同处理
         */
        if (LoginType.JSON.equals(securityProperties.getBrowser().getLoginType())) {
            //响应json
            //把authentication返回给响应
            response.setContentType("application/json;charset=UTF-8");
            response.getWriter().write(objectMapper.writeValueAsString(authentication));
        } else {
            //调用父类的方法，默认就是重定向
            super.onAuthenticationSuccess(request, response, authentication);
        }
    }
}
