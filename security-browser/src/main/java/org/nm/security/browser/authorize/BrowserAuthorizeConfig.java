package org.nm.security.browser.authorize;

import org.nm.security.core.authorize.AuthorizeConfigProvider;
import org.nm.security.core.properties.SecurityConstants;
import org.nm.security.core.properties.SecurityProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.stereotype.Component;

@Component
@Order(Integer.MIN_VALUE)
public class BrowserAuthorizeConfig implements AuthorizeConfigProvider {

    @Autowired
    private SecurityProperties securityProperties;

    @Override
    public void config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config) {
        config.antMatchers(
                SecurityConstants.DEFAULT_SESSION_INVALID_URL,
                securityProperties.getBrowser().getLogoutUrl()
        ).permitAll();
    }
}
