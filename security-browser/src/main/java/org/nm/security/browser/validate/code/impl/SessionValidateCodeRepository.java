package org.nm.security.browser.validate.code.impl;

import org.apache.commons.lang.StringUtils;
import org.nm.security.core.validate.code.ValidateCode;
import org.nm.security.core.validate.code.ValidateCodeException;
import org.nm.security.core.validate.code.ValidateCodeRepository;
import org.nm.security.core.validate.code.ValidateCodeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.social.connect.web.HttpSessionSessionStrategy;
import org.springframework.social.connect.web.SessionStrategy;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/12/23
 * Time: 15:18
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
@Component
public class SessionValidateCodeRepository implements ValidateCodeRepository {
    private Logger logger = LoggerFactory.getLogger(getClass());
    private SessionStrategy sessionStrategy = new HttpSessionSessionStrategy();

    /**
     * 验证码放入session时的前缀
     */
    String SESSION_KEY_PREFIX = "SESSION_KEY_FOR_CODE_";

    @Override
    public void save(ServletWebRequest request, ValidateCode validateCode, ValidateCodeType validateCodeType) {
        sessionStrategy.setAttribute(request, getSessionKey(request, validateCodeType, validateCode.getCode()), validateCode);
    }

    private String getSessionKey(ServletWebRequest request, ValidateCodeType validateCodeType, String code) {
        return SESSION_KEY_PREFIX + validateCodeType.toString().toUpperCase() + ":" + code.toUpperCase();
    }

    @Override
    public ValidateCode get(ServletWebRequest request, ValidateCodeType validateCodeType) {
        try {
            String codeInRequest = ServletRequestUtils.getStringParameter(request.getRequest(), validateCodeType.getParamNameOnValidate());
            if(StringUtils.isBlank(codeInRequest)){
                throw new ValidateCodeException("验证码不能为空");
            }
            Object val = sessionStrategy.getAttribute(request, getSessionKey(request, validateCodeType, codeInRequest));
            if (val != null) {
                return (ValidateCode) val;
            }
        } catch (ServletRequestBindingException e) {
            logger.error("获取validateCode错误：", e);
        }
        return null;
    }

    @Override
    public void remove(ServletWebRequest request, ValidateCodeType validateCodeType) {
        try {
            String codeInRequest = ServletRequestUtils.getStringParameter(request.getRequest(), validateCodeType.getParamNameOnValidate());
            sessionStrategy.removeAttribute(request, getSessionKey(request, validateCodeType, codeInRequest));
        } catch (ServletRequestBindingException e) {
            logger.error("获取validateCode错误：", e);
        }
    }
}
