package org.nm.security.browser;

import org.nm.security.core.authentication.mobile.SmsCodeAuthenticationSecurityConfig;
import org.nm.security.core.authorize.AuthorizeConfigManager;
import org.nm.security.core.properties.SecurityConstants;
import org.nm.security.core.properties.SecurityProperties;
import org.nm.security.core.validate.code.ValidateCodeSecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.session.InvalidSessionStrategy;
import org.springframework.security.web.session.SessionInformationExpiredStrategy;
import javax.sql.DataSource;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/11/21
 * Time: 22:57
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
@Configuration
public class BrowserSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private SecurityProperties securityProperties;

    @Autowired
    private AuthenticationSuccessHandler nmAuthenticationSuccessHandler;

    @Autowired
    private AuthenticationFailureHandler nmAuthenticationFailureHandler;

    @Autowired
    private ValidateCodeSecurityConfig validateCodeSecurityConfig;

    @Autowired
    private SmsCodeAuthenticationSecurityConfig smsCodeAuthenticationSecurityConfig;

    /*@Autowired
    private SpringSocialConfigurer nmSocialSecurityConfig;*/

    @Autowired
    private AuthorizeConfigManager authorizeConfigManager;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private SessionInformationExpiredStrategy sessionInformationExpiredStrategy;

    @Autowired
    private InvalidSessionStrategy invalidSessionStrategy;

    @Autowired
    private LogoutSuccessHandler logoutSuccessHandler;

    /**
     * 记住我TokenRepository配置，在登录成功后执行
     * 登录成功后往数据库存token的
     *
     * @return
     */
    @Bean
    public PersistentTokenRepository persistentTokenRepository() {
        JdbcTokenRepositoryImpl jdbcTokenRepository = new JdbcTokenRepositoryImpl();
        jdbcTokenRepository.setDataSource(dataSource);
        //启动时自动生成相应表，可以在JdbcTokenRepositoryImpl里自己执行CREATE_TABLE_SQL脚本生成表
        jdbcTokenRepository.setCreateTableOnStartup(securityProperties.getBrowser().isCreateRememberMeTable());
        return jdbcTokenRepository;
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //实现需要认证的接口跳转表单登录,安全=认证+授权
        //http.httpBasic() //这个就是默认的弹框认证
        http.apply(validateCodeSecurityConfig).//把验证码过滤器加载登录过滤器前边
                and()
                .apply(smsCodeAuthenticationSecurityConfig)//短信登录
                //.and()
                //.apply(nmSocialSecurityConfig)//第三方登录
                .and()
                .formLogin() //表单认证
                .loginPage(SecurityConstants.DEFAULT_UNAUTHENTICATION_URL)
                .loginProcessingUrl(SecurityConstants.DEFAULT_LOGIN_PROCESSING_URL_FORM)
                .successHandler(nmAuthenticationSuccessHandler) //自定义的认证后处理器
                .failureHandler(nmAuthenticationFailureHandler)
                .and()
                .rememberMe()
                .tokenRepository(persistentTokenRepository())//TokenRepository，登录成功后往数据库存token的
                .tokenValiditySeconds(securityProperties.getBrowser().getRememberMeSeconds())//记住我秒数
                .userDetailsService(userDetailsService) //记住我成功后，调用userDetailsService查询用户信息
                .and()
                .logout()
                .logoutUrl("/user/logout")
                .logoutSuccessHandler(logoutSuccessHandler)
                .and()
                .sessionManagement()
                .invalidSessionStrategy(invalidSessionStrategy)
                .maximumSessions(securityProperties.getBrowser().getSession().getMaximumSessions())
                .maxSessionsPreventsLogin(securityProperties.getBrowser().getSession().getMaxSessionPreventsLogin())
                .expiredSessionStrategy(sessionInformationExpiredStrategy)
                .and()
                .and()
                .csrf().disable();  //关闭csrf 防护
        //授权管理
        authorizeConfigManager.config(http.authorizeRequests());
    }
}
