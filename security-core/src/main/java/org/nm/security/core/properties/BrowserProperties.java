package org.nm.security.core.properties;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/11/23
 * Time: 22:06
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */

/**
 * 浏览器配置项
 */
public class BrowserProperties {
    //用户未配置默认登录页
    private String loginPage = "/login.html";

    //退出登录后的路径
    private String signUrl;

    //退出登录路径
    private String logoutUrl = "/user/logout";

    private String signUpUrl = "/nm-signUp.html";

    private LoginType loginType = LoginType.JSON;

    private int rememberMeSeconds = 1000;

    public String getLogoutUrl() {
        return logoutUrl;
    }

    public void setLogoutUrl(String logoutUrl) {
        this.logoutUrl = logoutUrl;
    }

    public String getSignUrl() {
        return signUrl;
    }

    public void setSignUrl(String signUrl) {
        this.signUrl = signUrl;
    }

    /**
     * 是否创建记住我的数据表
     */
    private boolean isCreateRememberMeTable = false;

    private SessionProperties session = new SessionProperties();

    public boolean isCreateRememberMeTable() {
        return isCreateRememberMeTable;
    }

    public void setCreateRememberMeTable(boolean createRememberMeTable) {
        isCreateRememberMeTable = createRememberMeTable;
    }

    public SessionProperties getSession() {
        return session;
    }

    public void setSession(SessionProperties session) {
        this.session = session;
    }

    public int getRememberMeSeconds() {
        return rememberMeSeconds;
    }

    public void setRememberMeSeconds(int rememberMeSeconds) {
        this.rememberMeSeconds = rememberMeSeconds;
    }

    public String getLoginPage() {
        return loginPage;
    }

    public void setLoginPage(String loginPage) {
        this.loginPage = loginPage;
    }

    public LoginType getLoginType() {
        return loginType;
    }

    public void setLoginType(LoginType loginType) {
        this.loginType = loginType;
    }

    public String getSignUpUrl() {
        return signUpUrl;
    }

    public void setSignUpUrl(String signUpUrl) {
        this.signUpUrl = signUpUrl;
    }
}
