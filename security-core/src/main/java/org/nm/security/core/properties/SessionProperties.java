package org.nm.security.core.properties;


public class SessionProperties {
    /**
     * 同一个用户在系统中的最大session，默认1
     */
    private Integer maximumSessions = 1;

    /**
     * 达到最大session 时是否阻止新的登录请求，默认为false，不阻止，新的登录会将老的登录失效掉
     */
    private Boolean isMaxSessionPreventsLogin = false;

    /**
     * session 失效时跳转的地址
     */
    private String sessionInvalidUrl = SecurityConstants.DEFAULT_SESSION_INVALID_URL;

    public String getSessionInvalidUrl() {
        return sessionInvalidUrl;
    }

    public void setSessionInvalidUrl(String sessionInvalidUrl) {
        this.sessionInvalidUrl = sessionInvalidUrl;
    }

    public Integer getMaximumSessions() {
        return maximumSessions;
    }

    public void setMaximumSessions(Integer maximumSessions) {
        this.maximumSessions = maximumSessions;
    }

    public Boolean getMaxSessionPreventsLogin() {
        return isMaxSessionPreventsLogin;
    }

    public void setMaxSessionPreventsLogin(Boolean maxSessionPreventsLogin) {
        isMaxSessionPreventsLogin = maxSessionPreventsLogin;
    }
}
