package org.nm.security.core.social.qq.api;

public interface QQ {

    /**
     * 获取qq用户信息
     *
     * @return
     * @throws Exception
     */
    QQUserInfo getUserInfo();
}
