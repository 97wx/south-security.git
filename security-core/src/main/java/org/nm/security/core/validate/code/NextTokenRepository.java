package org.nm.security.core.validate.code;

import org.springframework.web.context.request.ServletWebRequest;

/**
 * validate code 下一步token存储器
 */
public interface NextTokenRepository {

    /**
     * @param request
     */
    String save(ServletWebRequest request, ValidateCode code);

    /**
     * @param request
     */
    ValidateCode get(ServletWebRequest request);


    /**
     * @param request
     */
    void remove(ServletWebRequest request);

    /**
     * @param request
     * @return
     */
    boolean hashToken(ServletWebRequest request);
}
