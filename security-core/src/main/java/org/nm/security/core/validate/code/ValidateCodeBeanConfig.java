package org.nm.security.core.validate.code;

import org.nm.security.core.properties.SecurityProperties;
import org.nm.security.core.validate.image.ImageCodeGenerator;
import org.nm.security.core.validate.impl.DefaultNextTokenRepository;
import org.nm.security.core.validate.sms.DefaultSmsCodeSender;
import org.nm.security.core.validate.sms.SmsCodeSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/11/25
 * Time: 13:47
 * 码云：https://gitee.com/97wx/watched
 * ========================
 * 配置验证码生成接口ValidateCodeGenerator的实际实现类的Bean
 * ClassName: ValidateCodeBeanConfig
 *
 * @Description: 配置验证码生成接口ValidateCodeGenerator的实际实现类的Bean
 * 如图片验证码的实现、短信验证码的实现
 */

@Component
public class ValidateCodeBeanConfig {
    @Autowired
    private SecurityProperties securityProperties;

    @Bean
    @ConditionalOnMissingBean(name = "imageValidateCodeGenerator")
    public ValidateCodeGenerator imageValidateCodeGenerator() {
        ImageCodeGenerator codeGenerator = new ImageCodeGenerator();
        codeGenerator.setSecurityProperties(securityProperties);
        return codeGenerator;
    }

    @Bean
    @ConditionalOnMissingBean(SmsCodeSender.class)
    public SmsCodeSender smsCodeSender() {
        DefaultSmsCodeSender defaultSmsCodeSender = new DefaultSmsCodeSender();
        return defaultSmsCodeSender;
    }

    @Bean
    @ConditionalOnMissingBean(NextTokenRepository.class)
    public NextTokenRepository nextTokenRepository() {
        DefaultNextTokenRepository defaultNextTokenRepository = new DefaultNextTokenRepository(securityProperties);
        return defaultNextTokenRepository;
    }
}
