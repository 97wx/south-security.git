package org.nm.security.core.validate.code;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/11/25
 * Time: 15:12
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
@Component
public class ValidateCodeProcessorHolder {

    @Autowired
    private Map<String, ValidateCodeProcessor> validateCodeProsessorMap;

    public ValidateCodeProcessor findValidateCodeProsessor(ValidateCodeType type) {
        return findValidateCodeProsessor(type.toString().toLowerCase());
    }

    public ValidateCodeProcessor findValidateCodeProsessor(String type) {
        String name = type.toLowerCase() + ValidateCodeProcessor.class.getSimpleName();
        ValidateCodeProcessor validateCodeProsessor = validateCodeProsessorMap.get(name);
        if (validateCodeProsessor == null) {
            throw new ValidateCodeException("验证码处理器：" + name + "、不存在！");
        }
        return validateCodeProsessor;
    }

}
