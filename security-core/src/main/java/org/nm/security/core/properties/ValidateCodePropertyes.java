package org.nm.security.core.properties;

import lombok.Data;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/11/25
 * Time: 13:20
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
@Data
public class ValidateCodePropertyes {

    /**
     * 默认配置
     */
    private ImageCodeProperties image = new ImageCodeProperties();

    private SmsCodeProperties sms = new SmsCodeProperties();

    /**
     * 下一步校验的token名称
     */
    private String nextTokenName = SecurityConstants.DEFAULT_VALIDATE_CODE_NEED_NEXT_PARAMS_NAME;

    /**
     * 下一步校验时的token
     */
    private String tokenName = SecurityConstants.DEFAULT_NEXT_TOKEN_PARAMS_NAME;

    /**
     * token 拦截地址
     */
    private String tokenUrl = null;
}
