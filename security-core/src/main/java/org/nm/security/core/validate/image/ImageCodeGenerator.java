package org.nm.security.core.validate.image;

import org.nm.security.core.properties.SecurityProperties;
import org.nm.security.core.validate.code.ValidateCode;
import org.nm.security.core.validate.code.ValidateCodeGenerator;
import org.nm.security.core.validate.code.VerifyCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/11/25
 * Time: 13:47
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
public class ImageCodeGenerator implements ValidateCodeGenerator {

    @Autowired
    private SecurityProperties securityProperties;

    @Override
    public ValidateCode generator(ServletWebRequest request) {
        //先从request里读取有没有长、宽、字符个数参数，有的话就用，没有用默认的
        int width = ServletRequestUtils.getIntParameter(request.getRequest(), "width", securityProperties.getCode().getImage().getWidth());

        int height = ServletRequestUtils.getIntParameter(request.getRequest(), "height", securityProperties.getCode().getImage().getHeight());

        int charLength = this.securityProperties.getCode().getImage().getLength();
        VerifyCode verifyCode = new VerifyCode(width, height, charLength);
        return new ImageCode(verifyCode.getImage(), verifyCode.getText(), this.securityProperties.getCode().getImage().getExpireIn());
    }

    public SecurityProperties getSecurityProperties() {
        return securityProperties;
    }

    public void setSecurityProperties(SecurityProperties securityProperties) {
        this.securityProperties = securityProperties;
    }

}
