package org.nm.security.core.social.qq.connet;

import org.nm.security.core.social.qq.api.QQ;
import org.springframework.social.connect.support.OAuth2ConnectionFactory;

public class QQConnectionFactory extends OAuth2ConnectionFactory<QQ> {
    /**
     * * 需要两个对象：
     * 1，ServiceProvider --> QQServieProvider
     * 2，ApiAdapter      --> QQApiAdapter
     * <p>Description: </p>
     *
     * @param providerId
     * @param appId
     * @param appSecret
     */
    public QQConnectionFactory(String providerId, String appId, String appSecret) {
        super(providerId, new QQServiceProvider(appId, appSecret), new QQAdapter());

    }
}
