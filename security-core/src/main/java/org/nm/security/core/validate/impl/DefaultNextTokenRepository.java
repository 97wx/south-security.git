package org.nm.security.core.validate.impl;

import lombok.extern.slf4j.Slf4j;
import org.nm.security.core.properties.SecurityProperties;
import org.nm.security.core.validate.code.NextTokenRepository;
import org.nm.security.core.validate.code.ValidateCode;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.ServletWebRequest;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/***
 *
 * @Auther: guzhangwen
 * @BusinessCard https://blog.csdn.net/gu_zhang_w
 * @Date: 2019/3/16
 * @Time: 16:35
 * @version : V1.0
 */
@Slf4j
public class DefaultNextTokenRepository implements NextTokenRepository {

    private SecurityProperties securityProperties;

    public DefaultNextTokenRepository(SecurityProperties securityProperties) {
        this.securityProperties = securityProperties;
    }

    private static Map<String, ValidateCode> validateCodeMap = new HashMap<>();

    @Override
    public String save(ServletWebRequest request, ValidateCode code) {
        String token = UUID.randomUUID().toString();
        validateCodeMap.put(token, code);
        return token;
    }

    @Override
    public ValidateCode get(ServletWebRequest request) {
        String tokenName = securityProperties.getCode().getTokenName();
        try {
            String token = ServletRequestUtils.getStringParameter(request.getRequest(), tokenName);
            ValidateCode code = validateCodeMap.get(token);
            return code;
        } catch (ServletRequestBindingException e) {
            log.error("无法获取ValidateCode");
        }
        return null;
    }

    @Override
    public void remove(ServletWebRequest request) {
        String tokenName = securityProperties.getCode().getTokenName();
        try {
            String token = ServletRequestUtils.getStringParameter(request.getRequest(), tokenName);
            validateCodeMap.remove(token);
            log.debug("移除next token、token={}", token);
        } catch (ServletRequestBindingException e) {
            log.debug("移除next token失败");
        }
    }

    @Override
    public boolean hashToken(ServletWebRequest request) {
        String tokenName = securityProperties.getCode().getTokenName();
        try {
            String token = ServletRequestUtils.getStringParameter(request.getRequest(), tokenName);
            if (validateCodeMap.containsKey(token)) {
                log.debug("通过next token参数校验");
                return true;
            }
        } catch (ServletRequestBindingException e) {
            log.error("无法删除token");
        }
        return false;
    }
}
