package org.nm.security.core.validate.code;

import org.springframework.web.context.request.ServletWebRequest;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/11/25
 * Time: 13:46
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
public interface ValidateCodeGenerator {
    /**
     * 图片验证码生成接口
     *
     * @param @param  request
     * @param @return
     * @return ImageCode
     * @throws
     * @Description: TODO
     * @author lihaoyang
     * @date 2018年3月2日
     */
    ValidateCode generator(ServletWebRequest request);
}
