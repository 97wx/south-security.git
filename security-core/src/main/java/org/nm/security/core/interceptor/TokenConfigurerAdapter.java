package org.nm.security.core.interceptor;

import org.apache.commons.lang.StringUtils;
import org.nm.security.core.properties.SecurityProperties;
import org.nm.security.core.validate.code.NextTokenInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2019/3/17
 * Time: 23:49
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
@Configuration
public class TokenConfigurerAdapter extends WebMvcConfigurerAdapter {

    @Autowired
    private NextTokenInterceptor nextTokenInterceptor;

    @Autowired
    private SecurityProperties securityProperties;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        String urls = securityProperties.getCode().getTokenUrl();
        if (StringUtils.isNotBlank(urls)) {
            List<String> list = new ArrayList<>();
            for (String patter : urls.split(",")) {
                list.add(patter);
            }
            registry.addInterceptor(nextTokenInterceptor).addPathPatterns(list.toArray(new String[list.size()]));
        }
    }
}
