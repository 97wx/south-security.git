package org.nm.security.core.validate.sms;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/11/28
 * Time: 22:17
 * 码云：https://gitee.com/97wx/watched
 * ========================
 * 发送短信验证码
 */
public interface SmsCodeSender {

    /**
     * 发送短信验证码
     *
     * @param mobile
     * @param code
     */
    void send(String mobile, String code);
}
