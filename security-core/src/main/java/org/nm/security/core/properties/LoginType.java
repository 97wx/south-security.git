package org.nm.security.core.properties;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/11/23
 * Time: 23:02
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */

/**
 * 登录类型枚举
 */
public enum LoginType {
    /**
     * 返回json
     */
    JSON,

    /**
     * 重定向
     */
    REDIRECT
}
