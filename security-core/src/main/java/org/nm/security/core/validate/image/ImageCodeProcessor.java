package org.nm.security.core.validate.image;

import org.nm.security.core.validate.code.ValidateCodeProcessor;
import org.nm.security.core.validate.impl.AbstractValidateCodeProsessor;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;

import javax.imageio.ImageIO;
import java.io.IOException;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/11/25
 * Time: 15:07
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
@Component("imageValidateCodeProcessor")
public class ImageCodeProcessor extends AbstractValidateCodeProsessor<ImageCode> implements ValidateCodeProcessor {

    @Override
    protected void send(ServletWebRequest request, ImageCode imageCode) throws IOException {
        ImageIO.write(imageCode.getImage(), "JPEG", request.getResponse().getOutputStream());
    }

    @Override
    protected String getSender(ServletWebRequest request) {
        return null;
    }
}
