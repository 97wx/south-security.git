package org.nm.security.core.validate.impl;

import org.apache.commons.lang.StringUtils;
import org.nm.security.core.properties.SecurityProperties;
import org.nm.security.core.validate.code.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.ServletWebRequest;

import java.util.Map;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/11/25
 * Time: 14:51
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
public abstract class AbstractValidateCodeProsessor<C extends ValidateCode> implements ValidateCodeProcessor {
    @Autowired
    private NextTokenRepository nextTokenRepository;

    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 操作session的工具类
     */
    @Autowired
    private ValidateCodeRepository validateCodeRepository;

    /**
     * 收集系统中所有的 {@link ValidateCodeGenerator} 接口的实现。
     */
    @Autowired
    Map<String, ValidateCodeGenerator> validateCodeGeneratorMap;

    @Autowired
    private SecurityProperties securityProperties;

    @Override
    public void create(ServletWebRequest request) throws Exception {
        C validateCode = generate(request);
        validateCode.setSender(getSender(request));
        save(request, validateCode);
        send(request, validateCode);
    }

    /**
     * 发送校验码，由子类实现
     *
     * @param request
     * @param validateCode
     * @throws Exception
     */
    protected abstract void send(ServletWebRequest request, C validateCode) throws Exception;

    /**
     * 获取发送者
     *
     * @param request
     */
    protected abstract String getSender(ServletWebRequest request);

    private C generate(ServletWebRequest request) {
        String type = getValidateCodeType(request).toString().toLowerCase();
        String generatorName = type + ValidateCodeGenerator.class.getSimpleName();
        ValidateCodeGenerator validateCodeGenerator = validateCodeGeneratorMap.get(generatorName);
        if (validateCodeGenerator == null) {
            throw new ValidateCodeException("验证码生成器" + generatorName + "不存在");
        }
        return (C) validateCodeGenerator.generator(request);
    }

    private ValidateCodeType getValidateCodeType(ServletWebRequest request) {
        String type = StringUtils.substringBefore(getClass().getSimpleName(), "CodeProcessor");
        return ValidateCodeType.valueOf(type.toUpperCase());
    }

    /**
     * 保存校验码
     *
     * @param request
     * @param validateCode
     */
    private void save(ServletWebRequest request, C validateCode) {
        ValidateCode code = new ValidateCode(validateCode.getCode(), validateCode.getExpireTime(), validateCode.getSender());
        validateCodeRepository.save(request, code, getValidateCodeType(request));
    }


    @SuppressWarnings("unchecked")
    @Override
    public String validate(ServletWebRequest request) {
        ValidateCodeType processorType = getValidateCodeType(request);
        C codeInSession = (C) validateCodeRepository.get(request, getValidateCodeType(request));
        String codeInRequest;
        try {
            codeInRequest = ServletRequestUtils.getStringParameter(request.getRequest(),
                    processorType.getParamNameOnValidate());
        } catch (ServletRequestBindingException e) {
            throw new ValidateCodeException("获取验证码的值失败");
        }

        if (StringUtils.isBlank(codeInRequest)) {
            throw new ValidateCodeException(processorType + "验证码的值不能为空");
        }

        if (codeInSession == null) {
            throw new ValidateCodeException(processorType + "验证码不存在");
        }

        if (codeInSession.isExpried()) {
            validateCodeRepository.remove(request, getValidateCodeType(request));
            throw new ValidateCodeException(processorType + "验证码已过期");
        }

        if (!StringUtils.equalsIgnoreCase(codeInSession.getCode(), codeInRequest)) {
            throw new ValidateCodeException(processorType + "验证码不匹配");
        }

        String token = null;
        try {
            String nextTokenName = securityProperties.getCode().getNextTokenName();
            String needNext = ServletRequestUtils.getStringParameter(request.getRequest(), nextTokenName);
            if (StringUtils.isNotBlank(needNext)) {
                token = nextTokenRepository.save(request, codeInSession);
            }
        } catch (ServletRequestBindingException e) {
            logger.error("获取validateCode错误：", e);
        }
        validateCodeRepository.remove(request, getValidateCodeType(request));
        return token;
    }
}
