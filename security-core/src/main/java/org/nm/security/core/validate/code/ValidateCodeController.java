package org.nm.security.core.validate.code;

import org.nm.security.core.properties.SecurityConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/11/25
 * Time: 0:17
 * 码云：https://gitee.com/97wx/watched
 * ========================
 * 验证码Control
 */

@RestController
public class ValidateCodeController {

    @Autowired
    private ValidateCodeProcessorHolder validateCodeProcessorHolderl;

    @GetMapping(SecurityConstants.DEFAULT_VALIDATE_CODE_URL_PREFIX + "/{type}")
    public void createCode(HttpServletRequest request, HttpServletResponse response, @PathVariable String type) throws Exception {
        validateCodeProcessorHolderl.findValidateCodeProsessor(type).create(new ServletWebRequest(request, response));
    }

    @GetMapping(SecurityConstants.DEFAULT_VALIDATE_URL_PREFIX + "/{type}")
    public String validateCode(HttpServletRequest request, HttpServletResponse response, @PathVariable String type) throws Exception {
        return validateCodeProcessorHolderl.findValidateCodeProsessor(type).validate(new ServletWebRequest(request, response));
    }

}
