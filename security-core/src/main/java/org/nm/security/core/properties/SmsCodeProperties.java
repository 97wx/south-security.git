package org.nm.security.core.properties;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/11/25
 * Time: 13:20
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
public class SmsCodeProperties {

    //过期时间
    private int expireIn = 60;
    private int length = 6;
    private String url;


    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getExpireIn() {
        return expireIn;
    }

    public void setExpireIn(int expireIn) {
        this.expireIn = expireIn;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
