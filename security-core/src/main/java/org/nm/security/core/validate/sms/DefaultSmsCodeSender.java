package org.nm.security.core.validate.sms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/11/28
 * Time: 22:18
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
public class DefaultSmsCodeSender implements SmsCodeSender {

    private Logger logger = LoggerFactory.getLogger(getClass());


    @Override
    public void send(String mobile, String code) {
        logger.debug("发送短信验证码:#{},手机号：#{}", code, mobile);
    }
}
