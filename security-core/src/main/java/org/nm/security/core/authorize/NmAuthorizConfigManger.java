package org.nm.security.core.authorize;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 授权配置管理器
 * 1、通过管理器收集已经实现{@link AuthorizeConfigProvider} 接口的配置实现
 * 2、集中调用、将各个独立配置的授权配置聚合起来。统一管理
 */
@Component
public class NmAuthorizConfigManger implements AuthorizeConfigManager {

    /**
     * 这里不能用set 因为配置对加载顺序有要求
     */
    @Autowired
    private List<AuthorizeConfigProvider> authorizeConfigProviderList;

    @Override
    public void config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config) {
        for (AuthorizeConfigProvider provider : authorizeConfigProviderList) {
            provider.config(config);
        }
        //config.anyRequest().authenticated();
    }
}
