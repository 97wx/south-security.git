package org.nm.security.core.properties;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/11/25
 * Time: 13:20
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
public class ImageCodeProperties extends SmsCodeProperties {
    //图片宽
    private int width = 67;
    //图片高
    private int height = 23;

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
