package org.nm.security.core.validate.code;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/11/25
 * Time: 14:37
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
@Data
public class ValidateCode implements Serializable {

    private String code;

    private LocalDateTime expireTime;//过期时间点

    /**
     * 发送者
     */
    private String sender;

    public ValidateCode(String code, int expireIn) {
        this.code = code;
        this.expireTime = LocalDateTime.now().plusSeconds(expireIn);
    }

    public ValidateCode(String code, int expireIn, String sender) {
        this.code = code;
        this.expireTime = LocalDateTime.now().plusSeconds(expireIn);
        this.sender = sender;
    }

    public ValidateCode(String code, LocalDateTime expireTime) {
        this.code = code;
        this.expireTime = expireTime;
    }

    public ValidateCode(String code, LocalDateTime expireTime, String sender) {
        this.code = code;
        this.expireTime = expireTime;
        this.sender = sender;
    }

    public boolean isExpried() {
        return LocalDateTime.now().isAfter(expireTime);
    }
}
