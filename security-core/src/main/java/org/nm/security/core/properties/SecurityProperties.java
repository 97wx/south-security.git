package org.nm.security.core.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 自定义配置项
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/11/23
 * Time: 22:07
 * 码云：https://gitee.com/97wx/watched
 * ========================
 * nm.security.browser.loginPage = /demo-login.html
 * 其中的browser的配置会读取到BrowserProperties中去
 * 这是以点分割的，一级一级的和类的属性对应
 */
@ConfigurationProperties(prefix = "nm.security")
public class SecurityProperties {
    private BrowserProperties browser = new BrowserProperties();
    private ValidateCodePropertyes code = new ValidateCodePropertyes();
    private SocialProperties social = new SocialProperties();
    private OAuth2Properties oauth2 = new OAuth2Properties();

    public OAuth2Properties getOauth2() {
        return oauth2;
    }

    public void setOauth2(OAuth2Properties oauth2) {
        this.oauth2 = oauth2;
    }

    public SocialProperties getSocial() {
        return social;
    }

    public void setSocial(SocialProperties social) {
        this.social = social;
    }

    public BrowserProperties getBrowser() {
        return browser;
    }

    public void setBrowser(BrowserProperties browser) {
        this.browser = browser;
    }

    public ValidateCodePropertyes getCode() {
        return code;
    }

    public void setCode(ValidateCodePropertyes code) {
        this.code = code;
    }
}
