package org.nm.security.core.validate.sms;

import lombok.extern.slf4j.Slf4j;
import org.nm.security.core.properties.SecurityConstants;
import org.nm.security.core.validate.code.ValidateCode;
import org.nm.security.core.validate.code.ValidateCodeProcessor;
import org.nm.security.core.validate.impl.AbstractValidateCodeProsessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/11/28
 * Time: 22:30
 * 码云：https://gitee.com/97wx/watched
 * ========================
 */
@Slf4j
@Component("smsValidateCodeProcessor")
public class SmsCodeProcessor extends AbstractValidateCodeProsessor<ValidateCode> implements ValidateCodeProcessor {
    @Autowired
    private SmsCodeSender smsCodeSender;

    @Override
    protected void send(ServletWebRequest request, ValidateCode validateCode) throws Exception {
        smsCodeSender.send(validateCode.getCode(), getSender(request));
    }

    @Override
    protected String getSender(ServletWebRequest request) {
        String paramName = SecurityConstants.DEFAULT_PARAMETER_NAME_MOBILE;
        try {
            return ServletRequestUtils.getRequiredStringParameter(request.getRequest(), paramName);
        } catch (ServletRequestBindingException e) {
            log.debug("获取发送者失败");
        }
        return null;
    }
}
