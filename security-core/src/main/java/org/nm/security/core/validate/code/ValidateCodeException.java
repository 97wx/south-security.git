package org.nm.security.core.validate.code;

import org.springframework.security.core.AuthenticationException;

/**
 * ========================
 * Create with IntelliJ IDEA.
 * User: 我是顾子晨
 * Date：2018/11/25
 * Time: 0:15
 * 码云：https://gitee.com/97wx/watched
 * ========================
 * 验证码错误异常，继承spring security的认证异常
 */
public class ValidateCodeException extends AuthenticationException {
    /**
     * @Fields serialVersionUID : TODO
     */
    private static final long serialVersionUID = 1L;

    public ValidateCodeException(String msg) {
        super(msg);
    }
}
