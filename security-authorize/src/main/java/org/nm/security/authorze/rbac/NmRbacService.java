package org.nm.security.authorze.rbac;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

@Component("rbacService")
public class NmRbacService implements RbacService {

    private AntPathMatcher antPathMatcher = new AntPathMatcher();

    @Override
    public boolean hashPermision(HttpServletRequest request, Authentication authentication) {
        boolean hashPermision = true;


        Object principal = authentication.getPrincipal();
        String username = ((UserDetails) principal).getUsername();

        List<String> urls = Arrays.asList("/user/*");
        for (String url : urls) {
            if (antPathMatcher.match(url, request.getRequestURI())) {
                hashPermision = true;
                break;
            }
        }
        if (principal instanceof UserDetails) {
        }

        return hashPermision;
    }
}
