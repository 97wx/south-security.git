package org.nm.security.authorze.rbac;

import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;

public interface RbacService {

    /**
     * 是否有权限访问
     *
     * @param request
     * @param authentication
     * @return
     */
    boolean hashPermision(HttpServletRequest request, Authentication authentication);
}
