# south-security

### 项目介绍
spring security

### 软件架构
软件架构说明


### 安装教程

1. xxxx
2. xxxx
3. xxxx

### 使用说明

1. xxxx
2. xxxx
3. xxxx

### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

### oauth2

主要角色: 用户, 客户端, 认证服务器, 资源服务器


### 授权码模式

这个模式是完整,也最为严格遵守 oauth2 的协议  
grant_type=authorization_code  
client_id=由服务方发放

- 资源服务器为每个合法的客户端发放一个 client_id
- 客户端引导用户到认证服务器进行授权
- 用户允许登录后允许授权, 则回调客户请求过来携带的redirect_url, 并将授权码一起返回
- 客户端拿到授权码后, 携带授权码再次请求资源服务器获取 access_token
- 认证服务器确认无误后返回 access_token
- 客户端携带 access_token 请求资源服务器 
### 简化模式

这个模式缺少了"授权码"部分,所以称为简化模式  
grant_type=code

- 用户进入客户端,客户端将用户引导到认证服务器
- 用户进行授权后,将用户导向回调的 url 地址,url 地址中的hash部分包含了访问令牌
- 浏览器向资源服务器发起一个请求,不包含上一步中收到的 hash 值
- 资源服务器返回一个网页,其中包含的代码可以获取到hash值中的令牌
- 浏览器执行上一步的脚本,提出令牌
- 浏览器将令牌发给客户端
 
### 密码模式

这种模式需要将用户的帐号密码输入直接认证,一般用于内部系统  
grant_type=password

- 用户向客户端提供帐号密码
- 客户端将用户帐号密码发给认证服务器,后者请求令牌
- 服务器确认信息无误,返回令牌

### 客户商品模式

这种模式可以说不属于授权的范围  
grant_type=client_credentials

- 客户端向认证服务器进行身份认证，并要求一个访问令牌。
- 认证服务器确认无误后，向客户端提供访问令牌。

### sso 单点登录
1. 说见:[sso 单点登录](https://gitee.com/97wx/south-security/blob/master/sso-server/src/README.md)

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)